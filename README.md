# Embulk Docker image

This image build the embulk utility in a convenient docker image.

## Build

```
$ docker build . -t embulk
```

