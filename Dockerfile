FROM openjdk:8-jdk

ARG EMBULK_VERSION="0.9.17"
ARG EMBULK_SHA256SUM="595cf166d99f6a3650ac87390d64e230b0878f17a43cac275706c7f4a2fc6989"

ENV PATH="/embulk/bin:$PATH" \
  EMBULK_ROOT="/embulk"

WORKDIR /embulk

RUN curl --silent --create-dirs -o /embulk/bin/embulk -L "https://dl.embulk.org/embulk-${EMBULK_VERSION}.jar" \
  && (echo "${EMBULK_SHA256SUM}  bin/embulk" | sha256sum --check) \
  && chmod +x /embulk/bin/embulk

RUN embulk gem install \
  embulk-output-postgresql:0.8.7 \
  embulk-input-postgresql:0.10.1 \
  embulk-input-google_spreadsheets:1.1.1

COPY . /embulk

ENTRYPOINT ["/bin/bash", "/embulk/bin/embulk"]

